import java.io.File;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;

public class App {
    public static void main(String[] args) throws Exception {
        try {
            
            DocumentBuilderFactory dBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dBuilderFactory.newDocumentBuilder();
            Document document = dBuilder.newDocument();

            Element buecher = document.createElement("buecher");
            document.appendChild(buecher);

            Element buch = document.createElement("buch");
            buecher.appendChild(buch);

            Element titel = document.createElement("titel");
            buch.appendChild(titel);
            titel.appendChild(document.createTextNode("This is a fantastic Book"));

            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer();

            DOMSource source = new DOMSource(document);

            StreamResult result = new StreamResult(new File("buchhandlung.xml"));

            transformer.transform(source, result);

        } catch (Exception e) {
            e.printStackTrace();    
        }
    }
}
